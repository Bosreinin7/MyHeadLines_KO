//
//  File.swift
//  MyHeadlineApp
//
//  Created by Srei Nin on 3/6/18.
//  Copyright © 2018 Srei Nin. All rights reserved.
//

import Foundation
class Source{
    var id:String = ""
    var name:String = ""
    var description:String = ""
    init(sourceViewModel:SourceViewModel) {
        self.id = sourceViewModel.id!
        self.name = sourceViewModel.name!
        self.description = sourceViewModel.description!
    }
    init?(dictionary:[String:Any]){
        guard let id = dictionary["id"] as? String,
              let name = dictionary["name"] as? String,
              let description = dictionary["description"] as? String else {
            return nil
        }
        self.id = id
        self.name = name
        self.description = description
    }
    
    
}
