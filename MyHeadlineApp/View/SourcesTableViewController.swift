//
//  SourcesTableViewController.swift
//  MyHeadlineApp
//
//  Created by Srei Nin on 3/6/18.
//  Copyright © 2018 Srei Nin. All rights reserved.
//

import UIKit

class SourcesTableViewController: UITableViewController {
    private var webservice:Webservice!
    private var sourceListViewModel:SourceListViewModel!
    private var completion:() -> () = {}
    private var sourceViewModel:SourceViewModel!
    private var dataSource:TableViewDataSource<SourceTableViewCell,SourceViewModel>!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.webservice = Webservice()
        self.sourceListViewModel = SourceListViewModel(webservice: webservice){
            self.dataSource = TableViewDataSource(cellIdentifier: "cell", items: self.sourceListViewModel.sourcesViewModel){
                cell, vm in
                
                cell.titleLabel.text = vm.name
                cell.descriotionLabel.text = vm.description
            }
            self.tableView.dataSource = self.dataSource
            self.tableView.reloadData()
        }
      
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let cell = segue.destination as? HeadlinesTableViewController else {
            fatalError("HeadlinesTableViewController not found")
        }
        let indexPath = (self.tableView.indexPathForSelectedRow)!         
        let sourceViewModel = self.sourceListViewModel.sourceAt(indexPath: indexPath.row)
        cell.sourceViewModel = sourceViewModel
        
    }
  
}
