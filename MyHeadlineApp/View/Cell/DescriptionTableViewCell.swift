//
//  DescriptionTableViewCell.swift
//  MyHeadlineApp
//
//  Created by Srei Nin on 3/7/18.
//  Copyright © 2018 Srei Nin. All rights reserved.
//

import UIKit

class DescriptionTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var image_url: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }


}
