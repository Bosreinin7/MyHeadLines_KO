//
//  HeadlinesTableViewController.swift
//  MyHeadlineApp
//
//  Created by Srei Nin on 3/6/18.
//  Copyright © 2018 Srei Nin. All rights reserved.
//

import UIKit
import SDWebImage

class HeadlinesTableViewController: UITableViewController {
    
   var sourceViewModel:SourceViewModel!
    private var headlineListViewModels:HeadlinesListViewModel!
    private var dataSource:TableViewDataSource<DescriptionTableViewCell,HeadlineViewModel>!
    private var webservice:Webservice!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
        self.webservice = Webservice()
        self.headlineListViewModels = HeadlinesListViewModel(sourceViewModel: sourceViewModel){
            self.dataSource = TableViewDataSource(cellIdentifier: "cell", items: self.headlineListViewModels.headlineViewModels){
                cell, vm in
                cell.titleLabel.text = vm.title
                cell.descriptionLabel.text = vm.desccription
                cell.image_url.sd_setImage(with: URL(string: vm.urlToImage!), placeholderImage: UIImage(named: "images_not_avaliable.png"))
            }
            self.tableView.dataSource = self.dataSource
            self.tableView.reloadData()
        }
     
    }
    func updateUI(){
        self.title = sourceViewModel.name
    }
    

}
