//
//  Webservice.swift
//  MyHeadlineApp
//
//  Created by Srei Nin on 3/6/18.
//  Copyright © 2018 Srei Nin. All rights reserved.
//

import Foundation
class Webservice{
    var sources = [Source]()
    var headlines = [Headline]()
    func loadSource(complete:@escaping ([Source]) -> ()){
        let sourceURL = URL(string:"https://newsapi.org/v2/sources?apiKey=0cf790498275413a9247f8b94b3843fd")!
        URLSession.shared.dataTask(with: sourceURL){data, _, _ in
            if let data = data {
                let json = try! JSONSerialization.jsonObject(with: data, options: [])
                let dictionary = json as![String:Any]
                let sourceDictionary = dictionary["sources"] as! [[String: Any]]
                self.sources = sourceDictionary.flatMap(Source.init)
                DispatchQueue.main.async {
                    complete(self.sources)
                }
            
            }
        }.resume()
    }
    func loadHeadline(source:Source,complete:@escaping ([Headline]) -> ()){
        let headlineURL = URL(string:"https://newsapi.org/v2/top-headlines?sources=\(source.id)&apiKey=0cf790498275413a9247f8b94b3843fd")
        URLSession.shared.dataTask(with: headlineURL!){data,_ ,_ in
            if let data = data {
                let json = try! JSONSerialization.jsonObject(with: data, options: [])
                let dictionary = json as! [String:Any]
            let headlineDictionary = dictionary["articles"] as! [[String: Any]]
                self.headlines = headlineDictionary.flatMap(Headline.init)
                print("===Headline Sources==\(self.headlines.count)")
                DispatchQueue.main.sync {
                    complete(self.headlines)
                }
            }
            
        }.resume()
    }
    
}
