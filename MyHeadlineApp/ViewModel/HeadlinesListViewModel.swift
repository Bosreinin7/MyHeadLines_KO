//
//  HeadlinesListViewModel.swift
//  MyHeadlineApp
//
//  Created by Srei Nin on 3/7/18.
//  Copyright © 2018 Srei Nin. All rights reserved.
//

import Foundation
class HeadlinesListViewModel{
    private (set) var headlineViewModels:[HeadlineViewModel] = [HeadlineViewModel]()
    init(sourceViewModel:SourceViewModel,complete:@escaping () -> ()) {
       let source = Source(sourceViewModel: sourceViewModel)
        Webservice().loadHeadline(source:source){ headlines in
            self.headlineViewModels = headlines.map(HeadlineViewModel.init)
            DispatchQueue.main.async {
                complete()
            }
        }
    }
    func sourceAt(indexPath:Int) -> HeadlineViewModel{
        return headlineViewModels[indexPath]
    }
}
class HeadlineViewModel{
   var title:String?
   var desccription:String?
    var urlToImage:String?
    init(headline:Headline) {
        self.title = headline.title!
        self.desccription = headline.description!
        self.urlToImage = headline.urlToImage!
    }
}
